package com.diplomski.profile.business.model.factory;

import com.diplomski.profile.business.boundary.ProfileReadRepository;
import com.diplomski.profile.business.boundary.request.SignUpRequest;
import com.diplomski.profile.business.exceptions.EmailAlreadySignedUpException;
import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.valueobject.Email;
import com.diplomski.profile.business.model.valueobject.EncryptedPassword;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProfileFactory {

    public final ProfileReadRepository profileReadRepository;

    public Profile createProfile(final SignUpRequest signUpRequest) {
        final Email email = new Email(signUpRequest.getEmailString());
        if (profileReadRepository.existsByEmail(email)) {
            throw new EmailAlreadySignedUpException(email);
        }

        final EncryptedPassword encryptedPassword = EncryptedPassword
                .createFromPlainTextPassword(signUpRequest.getPlainTextPassword());

        final String id = profileReadRepository.generateNexId();

        return Profile.builder()
                .id(id)
                .email(email)
                .username(signUpRequest.getUserName())
                .encryptedPassword(encryptedPassword)
                .build();
    }
}

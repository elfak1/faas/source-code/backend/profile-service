package com.diplomski.profile.business.model.valueobject;

import com.diplomski.profile.business.model.Profile;
import io.jsonwebtoken.*;

import java.util.Date;
import java.util.Map;
import java.util.function.Function;

public class JWT {

    private static final long JWT_TOKEN_DURATION = 5 * 60 * 60;
    private static final String SECRET = "Vz783rK8PVuqQ6WuSGjMHaUh9iC39UNhQXfbwu4HVTfBY6gNE3SdRdqUNB4i7stJVWwWwHG5A4Sx3fyWUjGbuidZnrnzJvXpuLCG";
    private static final JwtParser JWT_PARSER = Jwts.parserBuilder().setSigningKey(SECRET).build();
    private static final String SUBJECT_ID_CLAIM_NAME = "sub-id";

    private final String token;

    public JWT(String token) {
        this.token = token;
    }

    public JWT(final Profile profile) {
        final Map<String, Object> claims = Map.of(
                SUBJECT_ID_CLAIM_NAME, profile.getId()
        );

        final Date issuedAt = new Date(System.currentTimeMillis());
        final Date expiration = new Date(System.currentTimeMillis() + JWT_TOKEN_DURATION * 1000);

        token = Jwts.builder()
                .setClaims(claims)
                .setSubject(profile.getEmail().toString())
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public String getEmailFromToken() {
        return getClaimFromToken(Claims::getSubject);
    }

    public Boolean isExpired() {
        final Date expiration = getClaimFromToken(Claims::getExpiration);
        return expiration.before(new Date());
    }

    private <T> T getClaimFromToken(Function<Claims, T> claimsResolver) {
        final Claims claims = JWT_PARSER
                .parseClaimsJws(token)
                .getBody();

        return claimsResolver.apply(claims);
    }

    @Override
    public String toString() {
        return token;
    }
}

package com.diplomski.profile.business.model.valueobject;

import com.diplomski.profile.business.exceptions.EmailNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.validator.routines.EmailValidator;

@Getter
@EqualsAndHashCode
public class Email {

    private static final EmailValidator emailValidator = EmailValidator.getInstance();

    private final String value;

    public Email(String value) {
        if (!emailValidator.isValid(value)) {
            throw new EmailNotValidException(value);
        }

        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}

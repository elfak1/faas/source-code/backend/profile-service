package com.diplomski.profile.business.exceptions;

public class WrongCredentialsException extends RuntimeException {

    public WrongCredentialsException() {
        super("Incorrect email or password");
    }
}

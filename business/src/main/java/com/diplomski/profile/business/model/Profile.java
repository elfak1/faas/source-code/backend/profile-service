package com.diplomski.profile.business.model;

import com.diplomski.profile.business.model.valueobject.Email;
import com.diplomski.profile.business.model.valueobject.EncryptedPassword;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Profile {

    @EqualsAndHashCode.Include
    private final String id;

    private final Email email;

    private final EncryptedPassword encryptedPassword;

    private final String username;

    public boolean hasPassword(final String plainTextPassword) {
        final EncryptedPassword encryptedPassword = EncryptedPassword.createFromPlainTextPassword(plainTextPassword);
        return this.encryptedPassword.equals(encryptedPassword);
    }
}

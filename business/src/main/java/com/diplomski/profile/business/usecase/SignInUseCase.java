package com.diplomski.profile.business.usecase;

import com.diplomski.profile.business.boundary.ProfileReadRepository;
import com.diplomski.profile.business.boundary.request.SignInRequest;
import com.diplomski.profile.business.boundary.response.SignInResponse;
import com.diplomski.profile.business.exceptions.WrongCredentialsException;
import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.valueobject.Email;
import com.diplomski.profile.business.model.valueobject.JWT;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignInUseCase {

    private final ProfileReadRepository profileReadRepository;

    public SignInResponse signIn(final SignInRequest signInRequest) {
        final Email email = new Email(signInRequest.getEmail());
        final Profile profile = profileReadRepository.findByEmail(email)
                .orElseThrow(WrongCredentialsException::new);

        if (!profile.hasPassword(signInRequest.getPlainTextPassword())) {
            throw new WrongCredentialsException();
        }

        final JWT jwt = new JWT(profile);
        return SignInResponse.builder()
                .jwt(jwt)
                .build();
    }
}

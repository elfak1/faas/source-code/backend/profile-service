package com.diplomski.profile.business.boundary.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignUpRequest {

    private final String userName;

    private final String emailString;

    private final String plainTextPassword;
}

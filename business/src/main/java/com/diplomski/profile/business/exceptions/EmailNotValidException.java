package com.diplomski.profile.business.exceptions;

public class EmailNotValidException extends RuntimeException{

    public EmailNotValidException(final String invalidEmail) {
        super(String.format("The provided email: '%s' is not valid.", invalidEmail));
    }
}

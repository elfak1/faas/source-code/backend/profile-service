package com.diplomski.profile.business.boundary.response;

import com.diplomski.profile.business.model.valueobject.JWT;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignInResponse {
    private final JWT jwt;
}

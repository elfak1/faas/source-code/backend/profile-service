package com.diplomski.profile.business.model.valueobject;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Getter
@EqualsAndHashCode
public class EncryptedPassword {
    //TODO: replace this with a real password encoder;
    private static final PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

    private final String value;

    public EncryptedPassword(final String value) {
        this.value = value;
    }

    public static EncryptedPassword createFromPlainTextPassword(final String plainTextPassword) {
        return new EncryptedPassword(passwordEncoder.encode(plainTextPassword));
    }

    @Override
    public String toString() {
        return value;
    }
}

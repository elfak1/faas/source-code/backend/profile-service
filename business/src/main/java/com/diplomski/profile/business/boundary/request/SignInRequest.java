package com.diplomski.profile.business.boundary.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignInRequest {
    private final String email;

    private final String plainTextPassword;
}

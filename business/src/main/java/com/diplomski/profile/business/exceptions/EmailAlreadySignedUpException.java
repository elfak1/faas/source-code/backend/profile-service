package com.diplomski.profile.business.exceptions;

import com.diplomski.profile.business.model.valueobject.Email;

public class EmailAlreadySignedUpException extends RuntimeException {

    public EmailAlreadySignedUpException(final Email email) {
        super(String.format("The provided email: '%s' is already signed up.", email));
    }
}

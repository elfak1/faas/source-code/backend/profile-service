package com.diplomski.profile.business.boundary;

import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.valueobject.Email;

import java.util.Optional;

public interface ProfileReadRepository {
    String generateNexId();

    boolean existsByEmail(final Email email);

    Optional<Profile> findByEmail(final Email email);
}

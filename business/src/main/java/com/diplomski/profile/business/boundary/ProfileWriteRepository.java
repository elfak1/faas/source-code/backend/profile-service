package com.diplomski.profile.business.boundary;

import com.diplomski.profile.business.model.Profile;

public interface ProfileWriteRepository {
    void persist(final Profile profile);
}

package com.diplomski.profile.business.usecase;

import com.diplomski.profile.business.boundary.ProfileWriteRepository;
import com.diplomski.profile.business.boundary.request.SignUpRequest;
import com.diplomski.profile.business.boundary.response.SignUpResponse;
import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.factory.ProfileFactory;
import com.diplomski.profile.business.model.valueobject.JWT;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignUpUseCase {

    private final ProfileFactory profileFactory;
    private final ProfileWriteRepository profileWriteRepository;

    public SignUpResponse signUp(final SignUpRequest signUpRequest) {
        final Profile profile = profileFactory.createProfile(signUpRequest);
        profileWriteRepository.persist(profile);
        final JWT jwt = new JWT(profile);

        return SignUpResponse.builder()
                .profile(profile)
                .jwt(jwt)
                .build();
    }
}

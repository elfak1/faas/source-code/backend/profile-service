package com.diplomski.profile.gateway.constants;

public class Endpoints {

    public static final String BASE_API = "/api/profile" ;
    public static final String PUBLIC_BASE = BASE_API + "/public";

    public static final String SIGN_UP = PUBLIC_BASE + "/sign-up";
    public static final String SIGN_IN = PUBLIC_BASE + "/sign-in";
    public static final String PING = BASE_API + "/ping";
}

package com.diplomski.profile.gateway.dto.request;

import com.diplomski.profile.business.boundary.request.SignUpRequest;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignUpRequestDTO {

    private final String userName;

    private final String email;

    private final String plainTextPassword;

    public SignUpRequest toSignUpRequest() {
        return SignUpRequest.builder()
                .userName(userName)
                .emailString(email)
                .plainTextPassword(plainTextPassword)
                .build();
    }
}

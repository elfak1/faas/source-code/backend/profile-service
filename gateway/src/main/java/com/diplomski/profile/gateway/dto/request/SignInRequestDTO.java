package com.diplomski.profile.gateway.dto.request;

import com.diplomski.profile.business.boundary.request.SignInRequest;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignInRequestDTO {

    private final String email;

    private final String plainTextPassword;

    public SignInRequest toSignInRequest() {
        return SignInRequest.builder()
                .email(email)
                .plainTextPassword(plainTextPassword)
                .build();
    }
}

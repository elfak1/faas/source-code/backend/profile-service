package com.diplomski.profile.gateway.controller.advisor;

import com.diplomski.profile.business.exceptions.EmailAlreadySignedUpException;
import com.diplomski.profile.business.exceptions.EmailNotValidException;
import com.diplomski.profile.business.exceptions.WrongCredentialsException;
import com.diplomski.profile.gateway.dto.response.base.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice(basePackages = {"com.diplomski.profile.gateway.controller"})
public class ProfileControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {EmailNotValidException.class})
    public ResponseEntity<BaseResponse> handleEmailNotValidException(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), exception.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {EmailAlreadySignedUpException.class})
    public ResponseEntity<BaseResponse> handleEmailAlreadySignedUpException(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT.getReasonPhrase(), exception.getMessage()),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {WrongCredentialsException.class})
    public ResponseEntity<BaseResponse> handleWrongCredentialsException(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase(), exception.getMessage()),
                HttpStatus.UNAUTHORIZED);
    }
}

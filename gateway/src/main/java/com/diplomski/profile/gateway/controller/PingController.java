package com.diplomski.profile.gateway.controller;

import com.diplomski.profile.gateway.dto.response.base.GenericDataResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.diplomski.profile.gateway.constants.Endpoints.PING;

@RestController
public class PingController {

    @GetMapping(PING)
    public GenericDataResponse<String> ping() {
        return new GenericDataResponse<>("Pong");
    }
}

package com.diplomski.profile.gateway.dto.response;

import com.diplomski.profile.business.boundary.response.SignUpResponse;
import com.diplomski.profile.gateway.dto.ProfileSignUpDTO;
import lombok.Getter;

@Getter
public class SignUpResponseDTO {

    private final ProfileSignUpDTO profile;

    private final String jwt;

    public SignUpResponseDTO(final SignUpResponse signUpResponse) {
        this.profile = new ProfileSignUpDTO(signUpResponse.getProfile());
        this.jwt = signUpResponse.getJwt().toString();
    }
}

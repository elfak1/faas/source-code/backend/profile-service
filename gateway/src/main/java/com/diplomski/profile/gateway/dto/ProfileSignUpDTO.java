package com.diplomski.profile.gateway.dto;

import com.diplomski.profile.business.model.Profile;
import lombok.Getter;

@Getter
public class ProfileSignUpDTO {

    private final String id;

    private final String email;

    private final String userNme;


    public ProfileSignUpDTO(final Profile profile) {
        this.id = profile.getId();
        this.email = profile.getEmail().toString();
        this.userNme = profile.getUsername();
    }
}

package com.diplomski.profile.gateway.dto.response;

import com.diplomski.profile.business.boundary.response.SignInResponse;
import lombok.Getter;

@Getter
public class SignInResponseDTO {

    private final String jwt;

    public SignInResponseDTO(final SignInResponse signInResponse) {
        jwt = signInResponse.getJwt().toString();
    }
}

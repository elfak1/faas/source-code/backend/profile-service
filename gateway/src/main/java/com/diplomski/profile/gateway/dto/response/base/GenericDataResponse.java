package com.diplomski.profile.gateway.dto.response.base;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class GenericDataResponse<T> extends BaseResponse {

    private final T data;

    public GenericDataResponse(
            T data,
            Integer statusCode,
            String error,
            String message) {
        super(statusCode, error, message);
        this.data = data;
    }

    public GenericDataResponse(
            T data,
            Integer statusCode) {
        super(statusCode);
        this.data = data;
    }
}
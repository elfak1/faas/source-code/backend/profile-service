package com.diplomski.profile.gateway.controller;

import com.diplomski.profile.business.boundary.request.SignInRequest;
import com.diplomski.profile.business.boundary.request.SignUpRequest;
import com.diplomski.profile.business.boundary.response.SignInResponse;
import com.diplomski.profile.business.boundary.response.SignUpResponse;
import com.diplomski.profile.business.usecase.SignInUseCase;
import com.diplomski.profile.business.usecase.SignUpUseCase;
import com.diplomski.profile.gateway.dto.request.SignInRequestDTO;
import com.diplomski.profile.gateway.dto.request.SignUpRequestDTO;
import com.diplomski.profile.gateway.dto.response.base.GenericDataResponse;
import com.diplomski.profile.gateway.dto.response.SignInResponseDTO;
import com.diplomski.profile.gateway.dto.response.SignUpResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.diplomski.profile.gateway.constants.Endpoints.SIGN_IN;
import static com.diplomski.profile.gateway.constants.Endpoints.SIGN_UP;

@RestController
@RequiredArgsConstructor
public class ProfileController {

    private final SignUpUseCase signUpUseCase;
    private final SignInUseCase signInUseCase;

    @PostMapping(SIGN_IN)
    public GenericDataResponse<SignInResponseDTO> signIn(@RequestBody SignInRequestDTO signInRequestDTO) {
        final SignInRequest signInRequest = signInRequestDTO.toSignInRequest();
        final SignInResponse signInResponse = signInUseCase.signIn(signInRequest);

        return new GenericDataResponse<>(new SignInResponseDTO(signInResponse));
    }

    @PostMapping(SIGN_UP)
    public GenericDataResponse<SignUpResponseDTO> signUp(@RequestBody SignUpRequestDTO signUpRequestDTO) {
        final SignUpRequest signUpRequest = signUpRequestDTO.toSignUpRequest();
        final SignUpResponse signUpResponse = signUpUseCase.signUp(signUpRequest);

        return new GenericDataResponse<>(new SignUpResponseDTO(signUpResponse));
    }
}

package com.diplomski.profile.persistence.entity.repository;

import com.diplomski.profile.persistence.entity.model.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProfileEntityRepository extends JpaRepository<ProfileEntity, Long> {

    @Query(nativeQuery = true, value = "select nextval('profile_id_seq')")
    Long getNextId();

    boolean existsByEmail(final String email);

    Optional<ProfileEntity> findByEmail(final String email);
}

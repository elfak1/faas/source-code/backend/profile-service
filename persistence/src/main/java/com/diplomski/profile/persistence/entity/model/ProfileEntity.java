package com.diplomski.profile.persistence.entity.model;

import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.valueobject.Email;
import com.diplomski.profile.business.model.valueobject.EncryptedPassword;
import com.diplomski.profile.persistence.util.TypeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "profile")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ProfileEntity extends BaseTimestampedEntity {

    @Id
    private Long id;

    private String email;

    private String encryptedPassword;

    private String username;

    public ProfileEntity(final Profile profile) {
        this.id = TypeConverter.string2Long(profile.getId());
        this.email = profile.getEmail().getValue();
        this.encryptedPassword = profile.getEncryptedPassword().getValue();
        this.username = profile.getUsername();
    }
}

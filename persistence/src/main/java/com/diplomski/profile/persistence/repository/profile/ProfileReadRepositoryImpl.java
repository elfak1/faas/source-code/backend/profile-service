package com.diplomski.profile.persistence.repository.profile;

import com.diplomski.profile.business.boundary.ProfileReadRepository;
import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.business.model.valueobject.Email;
import com.diplomski.profile.business.model.valueobject.EncryptedPassword;
import com.diplomski.profile.persistence.ProfileStorage;
import com.diplomski.profile.persistence.entity.model.ProfileEntity;
import com.diplomski.profile.persistence.entity.repository.ProfileEntityRepository;
import lombok.RequiredArgsConstructor;
import org.flywaydb.core.internal.jdbc.PlainExecutionTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class ProfileReadRepositoryImpl implements ProfileReadRepository {

    private final ProfileEntityRepository profileEntityRepository;

    @Override
    public String generateNexId() {
        return profileEntityRepository.getNextId().toString();
    }

    @Override
    public boolean existsByEmail(Email email) {
        return profileEntityRepository.existsByEmail(email.getValue());
    }

    @Override
    public Optional<Profile> findByEmail(Email email) {
        return profileEntityRepository.findByEmail(email.getValue())
                .map(this::toProfile);
    }

    //TODO : maybe move to converter
    private Profile toProfile(final ProfileEntity profileEntity) {
        return Profile.builder()
                .id(profileEntity.getId().toString())
                .username(profileEntity.getUsername())
                .email(new Email(profileEntity.getEmail()))
                .encryptedPassword(new EncryptedPassword(profileEntity.getEncryptedPassword()))
                .build();
    }
}

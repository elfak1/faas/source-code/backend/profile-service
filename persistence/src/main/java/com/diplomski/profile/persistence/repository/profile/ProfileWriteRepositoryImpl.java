package com.diplomski.profile.persistence.repository.profile;

import com.diplomski.profile.business.boundary.ProfileWriteRepository;
import com.diplomski.profile.business.model.Profile;
import com.diplomski.profile.persistence.entity.model.ProfileEntity;
import com.diplomski.profile.persistence.entity.repository.ProfileEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ProfileWriteRepositoryImpl implements ProfileWriteRepository {

    private final ProfileEntityRepository profileEntityRepository;

    @Override
    public void persist(Profile profile) {
        ProfileEntity profileEntity = new ProfileEntity(profile);

        profileEntityRepository.save(profileEntity);
    }
}

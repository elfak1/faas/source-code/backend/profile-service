package com.diplomski.profile.persistence.util;

import com.diplomski.profile.persistence.exception.TypeConversionException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TypeConverter {

    public static Long string2Long(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            throw new TypeConversionException(nfe);
        }
    }

    public static List<Long> strings2Longs(List<String> values) {
        return values.stream()
                .map(TypeConverter::string2Long)
                .collect(Collectors.toList());
    }

    public static Set<Long> strings2Longs(Set<String> values) {
        return values.stream()
                .map(TypeConverter::string2Long)
                .collect(Collectors.toSet());
    }
}

package com.diplomski.profile.configuration;

import com.diplomski.profile.gateway.constants.Endpoints;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().and().csrf().disable()
        .authorizeRequests()
        .antMatchers(Endpoints.SIGN_IN).permitAll()
        .antMatchers(Endpoints.SIGN_UP).permitAll()
        .antMatchers(Endpoints.PING).permitAll()
        .antMatchers("/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/v2/api-docs/**",
                "/swagger-ui/**",
                "/webjars/**").permitAll()
        .anyRequest().authenticated();
    }
}

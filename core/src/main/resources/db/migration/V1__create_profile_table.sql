CREATE TABLE profile
(
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id bigint NOT NULL,
	username character varying(1024),
    email character varying(1024),
	EncryptedPassword character varying,
    CONSTRAINT profile_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;